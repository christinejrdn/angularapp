var express = require('express');
var fs = require('fs');

var app = express();
var port = 8000;

var data = fs.readFileSync('./all-data.json');

app.all('*', function (request, response) {
  response.status(200).send(data);
});

app.listen(port, function () {
  console.log('Listening on port', port);
});
