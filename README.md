# About

## What does this app do?

It displays availability information from an instrument using the availability API, given its IP address.

## What do I need to run this app?

### For running

You must install [node.js](https://nodejs.org/)

then to install [grunt](http://gruntjs.com/) run:

`npm install -g grunt-cli`

then install cors-proxy (required to access the api programmatically):

`npm install -g corsproxy`

### For development

You can install [bower](http://bower.io/) by running:

`npm install -g bower`

## How do I run this app?

Run:

`grunt serve`

In a second console window run:

`corsproxy`

Your app is now accessible at http://localhost:9000 !

# Known Issues

This app was written to learn about angular. It has several known issues and does not work properly.

## 

# Need help?

You can contact me at christine at jrdn.ca and I'll try to get back ASAP!