/*global moment:false */
'use strict';

/**
 * @ngdoc function
 * @name ca.nanometrics.controllers.availability
 * @description
 * # AvailabilityController
 * Controller of the app
 */
angular.module('ca.nanometrics.controllers.availability', ['ca.nanometrics.services.availability'])
  .filter('nanodate', function() {
    return function (date) {
      return moment(date).format('MMMM Do, YYYY ') + date.substring(date.indexOf('T') + 1, date.length - 1);
    };
  })
  .controller('AvailabilityController', function ($scope, $log, AvailabilityService) {
    $scope.haveData = false;
    $scope.tabs = [];
    $scope.activeTabId = null;

    function findTab(tab) {
      for (var i = 0; i < $scope.tabs.length; i++) {
        if ($scope.tabs[i].data.id === tab.data.id)
        {
          return i;
        }
      }
      return -1;
    }

    $scope.addTab = function (tabData) {
      var tab = {
        data: tabData
      };
      var tabIndex = findTab(tab);
      if (tabIndex < 0) {
        tabIndex = $scope.tabs.push(tab) - 1;
        $log.debug('added tab for', tabData.id);
      }
      $scope.setActiveTab(tab);
    };

    $scope.setActiveTab = function (tab) {
      if (typeof tab === 'undefined') {
        $scope.activeTabId = null;
      } else {
        $scope.activeTabId = tab.data.id;
      }
    };

    $scope.$on('update:availability', function (event, data) {
      $scope.host = data.host;
      $scope.data = data.data;
      $scope.loading = false;
      $scope.haveData = true;
    });

    $scope.onKeypress = function ($event) {
      if ($event.keyCode === 13) {
        $scope.fetch();
      }
    };

    $scope.fetch = function () {
      AvailabilityService.fetch($scope.source);
    };
  });
