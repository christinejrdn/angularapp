'use strict';

angular.module('ca.nanometrics.controllers.page', [])
  .controller('PageController', function ($location, $log, $scope) {
    $scope.pages = [
      {
        name: 'Availability',
        path: '/availability'
      },
      {
        name: 'Data',
        path: '/data'
      }
    ];
    $scope.currentPage = null;

    var locationPath = $location.path();
    var locationPage = $scope.pages.filter(function (page) {
      return (page.path === locationPath);
    });

    if (locationPage.length > 0) {
      $scope.currentPage = locationPage[0];
    } else {
      $scope.currentPage = $scope.pages[0];
      $location.path($scope.currentPage.path);
    }

    $scope.setPage = function (page) {
      if ($scope.pages.indexOf(page) !== -1) {
        $scope.currentPage = page;
        $location.path(page.path);
      }
    };

    $scope.isCurrentPage = function (name) {
      return ($scope.currentPage.name === name);
    };
  });
