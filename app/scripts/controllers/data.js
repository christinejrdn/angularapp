/*global moment:false */
'use strict';

/**
 * @ngdoc function
 * @name ca.nanometrics.controllers.data
 * @description
 * # DataController
 * Controller of the app
 */
angular.module('ca.nanometrics.controllers.data', ['ca.nanometrics.services.data'])
  .controller('DataController', function ($scope, $log, DataService) {
    $scope.loading = true;

    $scope.endDate = moment.utc();
    $scope.startDate = moment.utc().startOf('day');

    $scope.startDateChanged = function(date) {
      $scope.startDate = date;
    };

    $scope.endDateChanged = function(date) {
      $scope.endDate = date;
    };

    $scope.channels = [];

    $scope.channelsChanged = function (channels) {
      $scope.channels = channels;
    };

    $scope.$on('update:data', function (event, data) {
      $scope.data = data;
      $scope.loading = false;
    });

    $scope.download = function () {
      DataService.download($scope.source, $scope.startDate, $scope.endDate, $scope.channels);
    };
  });
