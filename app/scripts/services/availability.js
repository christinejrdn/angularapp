'use strict';

/**
 * @ngdoc function
 * @name ca.nanometrics.services.availability
 * @description
 * # Availability
 * Service for getting availability
 */
angular.module('ca.nanometrics.services.availability', [])
  .factory('AvailabilityService', function ($http, $log, $q, $rootScope) {
    var apiPath = '/api/v1/channels/availability?';
    var testPath = '/fakeapi/blah';
    var that = {};

    that.data = {};

    that.fetch = function (host) {
      var url;
      if (typeof host !== 'string' || host.length === 0) {
        url = testPath;
      } else {
        url = host + apiPath;
      }
      $log.debug('fetching availability from ' + url);
      $http.get(url)
        .success(function (data) {
          that.data = {
            host: host,
            data: data
          };
          $rootScope.$broadcast('update:availability', that.data);
        })
        .error(function (error) {
          $log.error('Failed to get data availability: ' + error);
        });
    };

    return that;
  });
