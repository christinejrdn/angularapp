'use strict';

angular.module('ca.nanometrics.services.data', [])
  .factory('DataService', function ($http, $log, $q, $rootScope) {
    var apiPath = '/fdsnws/dataselect/1/query?';
    var testPath = '/fakeapi/blah';
    var that = {};

    that.data = {};

    that.download = function (host, startTime, endTime, channelPaths) {
      var url;
      if (typeof host !== 'string' || host.length === 0) {
        url = testPath;
      } else {
        url = host + apiPath;
      }

      var postData = createPostData(startTime, endTime, channelPaths);
      $log.debug('fetching data from ' + url, postData);
      $http.post(url, postData.join('\n'))
        .success(function (data) {
          that.data = {
            host: host,
            data: data
          };
          $rootScope.$broadcast('update:data', that.data);
        })
        .error(function (error) {
          $log.error('Failed to get data: ' + error);
        });
    };

    function createPostData(startTime, endTime, channelPaths) {
      var postData = [];
      createQueries(startTime, endTime, channelPaths).forEach(function (query) {
        postData.push(query.network + ' ' + query.station + ' ' + query.location + ' ' + query.channel + ' ' + query.starttime + ' ' + query.endtime);
      });
      return postData;
    }

    function createQueries(startTime, endTime, channelPaths) {
      var queries = [];
      channelPaths.forEach(function (channelPath) {
        queries.push(createQueryParameters(startTime, endTime, channelPath));
      });
      return queries;
    }

    function createQueryParameters(startTime, endTime, channelPath) {
      var channelPathParameters = channelPath.split('.');
      var network = channelPathParameters[0];
      var station = channelPathParameters[1];
      var location;
      var channel;
      if (channelPathParameters.length === 3)
      {
        location = '--';
        channel = channelPathParameters[2];
      }
      else if (channelPathParameters.length === 4)
      {
        location = channelPathParameters[2];
        channel = channelPathParameters[3];
      }
      return {
        network: sanitize(network),
        station: sanitize(station),
        location: sanitize(location),
        channel: sanitize(channel),
        starttime: startTime.format(),
        endtime: endTime.format()
      };
    }

    function sanitize(parameter) {
      return parameter.replace('|', ',');
    }

    return that;
  });
