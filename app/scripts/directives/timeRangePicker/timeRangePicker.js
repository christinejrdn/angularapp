'use strict';

angular.module('ca.nanometrics.directives.timeRangePicker', [])
  .directive('timeRangePicker', function () {
    return {
      restrict: 'E',
      templateUrl: 'scripts/directives/timeRangePicker/timeRangePicker.html',
      scope: {
        startDate: '=',
        endDate: '=',
        startDateChanged: '&',
        endDateChanged: '&'
      },
      link: function (scope, element) {
        var startElement = element.find('.start-date');
        var endElement = element.find('.end-date');

        startElement.datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss.SSS',
          defaultDate: scope.startDate,
          maxDate: scope.endDate,
          icons: { date: 'fa fa-calendar' },
          widgetPositioning: { vertical: 'auto', horizontal: 'right' }
        });
        startElement.on('dp.change', function (e) {
          if (typeof e.date !== 'object') {
            var endDate = moment(endElement.data('DateTimePicker').date()).utc();
            startElement.data('DateTimePicker').date(endDate.startOf('day'));
          } else {
            endElement.data('DateTimePicker').minDate(e.date);
            scope.startDateChanged({date: e.date});
          }
        });

        endElement.datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss.SSS',
          defaultDate: scope.endDate,
          icons: { date: 'fa fa-calendar' },
          widgetPositioning: { vertical: 'auto', horizontal: 'right' }
        });
        endElement.on('dp.change', function (e) {
          if (typeof e.date !== 'object') {
            endElement.data('DateTimePicker').date(moment.utc());
          } else {
            startElement.data('DateTimePicker').maxDate(e.date);
            scope.endDateChanged({date: e.date});
          }
        });
      }
    };
  });
