'use strict';

angular.module('ca.nanometrics.directives.tabBar', [])
  .directive('tabBar', function () {
    return {
      restrict: 'E',
      templateUrl: 'scripts/directives/tabBar/tabBar.html',
      scope: {
        tabs: '=',
        activeTabId: '=',
        setActiveTab: '&'
      },
      link: function (scope, element, attrs) {
        var increment = '=50px';
        var scrolling = false;
        var listElement = element.find('ul');

        function scrollTabs(param) {
          listElement.animate({ scrollLeft: param }, 'fast', 'linear', function () {
            if (scrolling) {
              scrollTabs(param);
            }
          });
        }

        scope.removeTab = function (tab) {
          var tabId = tab.data.id;
          var length = scope.tabs.length;
          for (var i = 0; i < length; i++) {
            if (scope.tabs[i].data.id === tab.data.id) {
              scope.tabs.splice(i, 1);
              break;
            }
          }
          if (tabId === scope.activeTabId) {
            if (scope.tabs.length > 0) {
              scope.setActiveTab(scope.tabs[0]);
            } else {
              scope.setActiveTab();
            }
          }
        };

        scope.tabLeftMousedown = function () {
          scrolling = true;
          scrollTabs('-' + increment);
        };

        scope.tabLeftMouseup = function () {
          scrolling = false;
        };

        scope.tabRightMousedown = function () {
          scrolling = true;
          scrollTabs('+' + increment);
        };

        scope.tabRightMouseup = function () {
          scrolling = false;
        };
      }
    };
  });
