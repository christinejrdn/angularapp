'use strict';

angular.module('ca.nanometrics.directives.channelBox', [])
  .directive('channelBox', function () {
    return {
      restrict: 'E',
      templateUrl: 'scripts/directives/channelBox/channelBox.html',
      scope: {
        channels: '=',
        channelsChanged: '&'
      },
      link: function (scope, element, attributes) {
        var input = element.find('input');
        var regex = /^[a-zA-Z0-9\*\?]{1,2}(\|[a-zA-Z0-9\*\?]{1,2})*\.[a-zA-Z0-9\*\?]{1,5}(\|[a-zA-Z0-9\*\?]{1,5})*(\.[a-zA-Z0-9\*\?]{1,2}(\|[a-zA-Z0-9\*\?]{1,2})*)?\.[a-zA-Z0-9\*\?]{1,3}(\|[a-zA-Z0-9\*\?]{1,3})*$/;
        input.tagsinput({
          confirmKeys: [13, 32, 188]
        });
        input.tagsinput('add', scope.channels.join(','));
        input.on('itemAdded', function () {
          scope.channelsChanged({ channels: input.tagsinput('items') });
        });
        input.on('itemRemoved', function () {
          scope.channelsChanged({ channels: input.tagsinput('items') });
        });
        input.on('beforeItemAdd', function (event) {
          event.cancel = !regex.test(event.item);
        });

        var box = element.find('.bootstrap-tagsinput');
        var boxInput = box.find('input');
        boxInput.focusin(function () {
          box.addClass('focus');
        });
        boxInput.focusout(function () {
          box.removeClass('focus');
        });
      }
    };
  });
