'use strict';

angular.module('ca.nanometrics.directives.inputGroupButton', [])
  .directive('inputGroupButton', function () {
    return {
      restrict: 'C',
      link: function (scope, element) {
        var input = element.find('input');
        var button = element.find('.input-group-addon, .input-group-btn');

        input.focusin(function () {
          button.addClass('focus');
        });
        input.focusout(function () {
          button.removeClass('focus');
        });
      }
    };
  });
