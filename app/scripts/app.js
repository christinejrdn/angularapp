'use strict';

/**
 * @ngdoc overview
 * @name ca.nanometrics
 * @description
 * # ca.nanometrics
 *
 * Main module of the application.
 */
angular.module('ca.nanometrics', [
  'ca.nanometrics.directives.inputGroupButton',
  'ca.nanometrics.directives.tabBar',
  'ca.nanometrics.directives.timeRangePicker',
  'ca.nanometrics.directives.channelBox',
  'ca.nanometrics.services.availability',
  'ca.nanometrics.services.data',
  'ca.nanometrics.controllers.page',
  'ca.nanometrics.controllers.availability',
  'ca.nanometrics.controllers.data',
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngTouch',
  'picardy.fontawesome',
  'ui.bootstrap',
  'mj.scrollingTabs'
])
.config(function () {

});
